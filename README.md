CRYPTO-WALLET API DOC
***
 Uygulamanın başlaması için MySQL bağlantısına ihtiyaç duyulmaktadır.
 MySQL bağlantısı kullanıcı adı : root şifre : root.
 
***
1) http://localhost:8080/swagger-ui/ adresinden Swagger
dökümantasyonu incelenebilir.
2) Uygulama ilk açıldığında içinde 1000 Tether bulunan ve 1
kullanıcıya ait 1 cüzdan oluşturulur.
3) Uygulama ayağa kalktığında 1 kullanıcı oluşturulmuş olur.
4) http://localhost:8080/wallet-user/list adresinden tüm kullanıcılar,
5) http://localhost:8080/wallet-user/list/{id} adresinden belirli
kullanıcılar sorgulanabilir.
6) localhost:8080/operation/buy?coin=<coin-name>&amount=<d
ouble-amount> adresinden coin adı(bitcoin veya ethereum) ve
double cinsten miktar girilerek coin alınır. İlgili coin alındıktan sonra
cüzdana eklenir ve harcanılan miktar cüzdandaki tether üzerinden
azalır.
7) localhost:8080/operation/sell?coin=<coin-name>&amount=<d
ouble-amount> adresinden coin adı(bitcoin veya ethereum) ve
double cinsten miktar girilerek coin satılır. İlgili coin satıldıktan
sonra cüzdandan miktarı azalır ve satılan miktar cüzdandaki tether
miktarının artışına dönüşür.
8) localhost:8080/operation/current-price?coin=bitcoin (veya
ethereum) bu adresle sistemdeki o anki istenilen coinin fiyatını
gösterir.
9) localhost:8080/operation/transaction?startDate=2021-03-17&e
ndDate=2021-06-19 (startDate ve endDate bilgisi şekildeki gibi
girilerek) yapılmış olan işlem geçmişlerine (alış ve satış) ait
transaction görüntülemesi yapılabilir. İstenilirse sadece başlangıç
tarihi verilip o tarihten itibaren olan kayıtlar veya bitiş tarihi
verilerek sadece o bitiş tarihinden önceki kayıtlar veya istenilirse
hiç tarih verilmeden tüm kayıtlar listelenebilir.